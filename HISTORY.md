# HISTORY

## 2019-08-15

### Epub

- [靠廢柴技能【狀態異常】成為最強的我將蹂躪一切](dmzj/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87) - dmzj
  <br/>( v: 1 , c: 13, add: 13 )

## 2019-08-14

### Epub

- [世界最強の後衛　～迷宮国の新人探索者～](syosetu/%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7%E3%81%AE%E5%BE%8C%E8%A1%9B%E3%80%80%EF%BD%9E%E8%BF%B7%E5%AE%AE%E5%9B%BD%E3%81%AE%E6%96%B0%E4%BA%BA%E6%8E%A2%E7%B4%A2%E8%80%85%EF%BD%9E) - syosetu
  <br/>( v: 4 , c: 20, add: 18 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 52, add: 2 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 2 )

## 2019-08-13

### Epub

- [不是真正同伴的我被逐出了勇者隊伍，因此決定在邊境過上慢生活](dmzj_out/%E4%B8%8D%E6%98%AF%E7%9C%9F%E6%AD%A3%E5%90%8C%E4%BC%B4%E7%9A%84%E6%88%91%E8%A2%AB%E9%80%90%E5%87%BA%E4%BA%86%E5%8B%87%E8%80%85%E9%9A%8A%E4%BC%8D%EF%BC%8C%E5%9B%A0%E6%AD%A4%E6%B1%BA%E5%AE%9A%E5%9C%A8%E9%82%8A%E5%A2%83%E9%81%8E%E4%B8%8A%E6%85%A2%E7%94%9F%E6%B4%BB) - dmzj_out
  <br/>( v: 4 , c: 38, add: 17 )
- [八男？別鬧了！](mirronight/%E5%85%AB%E7%94%B7%EF%BC%9F%E5%88%A5%E9%AC%A7%E4%BA%86%EF%BC%81) - mirronight
  <br/>( v: 1 , c: 203, add: 0 )
- [異世界薬局](syosetu/%E7%95%B0%E4%B8%96%E7%95%8C%E8%96%AC%E5%B1%80) - syosetu
  <br/>( v: 5 , c: 82, add: 0 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 50, add: 2 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 3 , c: 35, add: 0 )
- [中了40億圓樂透的我要搬到異世界去住了](wenku8_out/%E4%B8%AD%E4%BA%8640%E5%84%84%E5%9C%93%E6%A8%82%E9%80%8F%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86) - wenku8_out
  <br/>( v: 10 , c: 236, add: 0 )

### Segment

- [八男？別鬧了！](mirronight/%E5%85%AB%E7%94%B7%EF%BC%9F%E5%88%A5%E9%AC%A7%E4%BA%86%EF%BC%81) - mirronight
  <br/>( s: 1 )
- [異世界薬局](syosetu/%E7%95%B0%E4%B8%96%E7%95%8C%E8%96%AC%E5%B1%80) - syosetu
  <br/>( s: 21 )

## 2019-08-12

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 48, add: 2 )
- [中了40億圓樂透的我要搬到異世界去住了](wenku8_out/%E4%B8%AD%E4%BA%8640%E5%84%84%E5%9C%93%E6%A8%82%E9%80%8F%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86) - wenku8_out
  <br/>( v: 10 , c: 236, add: 0 )

## 2019-08-11

### Epub

- [八男？別鬧了！](mirronight/%E5%85%AB%E7%94%B7%EF%BC%9F%E5%88%A5%E9%AC%A7%E4%BA%86%EF%BC%81) - mirronight
  <br/>( v: 1 , c: 203, add: 203 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 46, add: 5 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 3 , c: 35, add: 2 )
- [中了40億圓樂透的我要搬到異世界去住了](wenku8_out/%E4%B8%AD%E4%BA%8640%E5%84%84%E5%9C%93%E6%A8%82%E9%80%8F%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86) - wenku8_out
  <br/>( v: 10 , c: 236, add: 0 )

### Segment

- [八男？別鬧了！](mirronight/%E5%85%AB%E7%94%B7%EF%BC%9F%E5%88%A5%E9%AC%A7%E4%BA%86%EF%BC%81) - mirronight
  <br/>( s: 146 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 2 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( s: 1 )
- [中了40億圓樂透的我要搬到異世界去住了](wenku8/%E4%B8%AD%E4%BA%8640%E5%84%84%E5%9C%93%E6%A8%82%E9%80%8F%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86) - wenku8
  <br/>( s: 23 )

## 2019-08-10

### Epub

- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( v: 2 , c: 37, add: 6 )
- [給鍬形蟲一記手刀結果穿越時空了](girl/%E7%B5%A6%E9%8D%AC%E5%BD%A2%E8%9F%B2%E4%B8%80%E8%A8%98%E6%89%8B%E5%88%80%E7%B5%90%E6%9E%9C%E7%A9%BF%E8%B6%8A%E6%99%82%E7%A9%BA%E4%BA%86) - girl
  <br/>( v: 1 , c: 5, add: 5 )
- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( v: 3 , c: 67, add: 2 )
- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( v: 6 , c: 49, add: 3 )
- [中了40億圓樂透的我要搬到異世界去住了](wenku8_out/%E4%B8%AD%E4%BA%8640%E5%84%84%E5%9C%93%E6%A8%82%E9%80%8F%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86) - wenku8_out
  <br/>( v: 10 , c: 236, add: 8 )

### Segment

- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( s: 4 )
- [給鍬形蟲一記手刀結果穿越時空了](girl/%E7%B5%A6%E9%8D%AC%E5%BD%A2%E8%9F%B2%E4%B8%80%E8%A8%98%E6%89%8B%E5%88%80%E7%B5%90%E6%9E%9C%E7%A9%BF%E8%B6%8A%E6%99%82%E7%A9%BA%E4%BA%86) - girl
  <br/>( s: 4 )
- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( s: 4 )
- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( s: 3 )

## 2019-08-09

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 41, add: 1 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 3 , c: 33, add: 1 )

## 2019-08-08

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 40, add: 1 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 3 , c: 32, add: 1 )

## 2019-08-07

### Epub

- [銜尾蛇的紀錄](syosetu/%E9%8A%9C%E5%B0%BE%E8%9B%87%E7%9A%84%E7%B4%80%E9%8C%84) - syosetu
  <br/>( v: 8 , c: 90, add: 0 )
- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( v: 6 , c: 46, add: 1 )
- [葉隠桜は嘆かない](ts/%E8%91%89%E9%9A%A0%E6%A1%9C%E3%81%AF%E5%98%86%E3%81%8B%E3%81%AA%E3%81%84) - ts
  <br/>( v: 3 , c: 63, add: 0 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 39, add: 1 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 3 , c: 31, add: 6 )

### Segment

- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( s: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( s: 2 )

## 2019-08-06

### Epub

- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( v: 6 , c: 45, add: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 3 , c: 38, add: 3 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 2 , c: 25, add: 10 )

### Segment

- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( s: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 2 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( s: 8 )

## 2019-08-04

### Epub

- [銜尾蛇的紀錄](syosetu/%E9%8A%9C%E5%B0%BE%E8%9B%87%E7%9A%84%E7%B4%80%E9%8C%84) - syosetu
  <br/>( v: 8 , c: 90, add: 90 )
- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( v: 5 , c: 44, add: 2 )
- [葉隠桜は嘆かない](ts/%E8%91%89%E9%9A%A0%E6%A1%9C%E3%81%AF%E5%98%86%E3%81%8B%E3%81%AA%E3%81%84) - ts
  <br/>( v: 3 , c: 63, add: 1 )

### Segment

- [銜尾蛇的紀錄](syosetu/%E9%8A%9C%E5%B0%BE%E8%9B%87%E7%9A%84%E7%B4%80%E9%8C%84) - syosetu
  <br/>( s: 6 )
- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( s: 2 )
- [葉隠桜は嘆かない](ts/%E8%91%89%E9%9A%A0%E6%A1%9C%E3%81%AF%E5%98%86%E3%81%8B%E3%81%AA%E3%81%84) - ts
  <br/>( s: 3 )

## 2019-08-03

### Epub

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( v: 3 , c: 65, add: 1 )
- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( v: 5 , c: 42, add: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 3 , c: 35, add: 2 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 2 , c: 15, add: 2 )

### Segment

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( s: 1 )
- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( s: 1 )

## 2019-08-02

### Epub

- [惡役轉生但是為什麼會變成這樣](girl_out/%E6%83%A1%E5%BD%B9%E8%BD%89%E7%94%9F%E4%BD%86%E6%98%AF%E7%82%BA%E4%BB%80%E9%BA%BC%E6%9C%83%E8%AE%8A%E6%88%90%E9%80%99%E6%A8%A3) - girl_out
  <br/>( v: 13 , c: 148, add: 0 )



