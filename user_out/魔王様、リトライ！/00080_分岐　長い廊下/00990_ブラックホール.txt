大野晶が、長い廊下を歩いている。
そこは巨大な高層ビルの最上階。中に入っているのは、誰でも知っているような大企業のオフィスばかりであった。

その廊下が、長い。
しかも、酷い眩暈がする。
真っ直ぐに歩いているのか、それとも曲がって歩いているのか？

そんな認識をする事すら難しく、視界には酷いノイズまで走っていた。
急に吐き氣が込み上げてきたのか、フラつく足取りでトイレへと向かうが、長い廊下の奧にあるのか、その距離すら酷く遠い。

（何だ、これ⋯⋯）

トイレの前に辿り着いた大野晶は、そこで輕い衝擊を受けた。
男子トイレと、女子トイレのマークが、見えないのだ。四角のプレートの中に何かの絵柄があるのは分かったのだが、色が見えない。
男子ならば青であり、女子ならば赤だろう。酷いノイズに阻まれ、そんな色さえ認識出来なかったのだ。

（ヤバ、吐きそうだわ、これ⋯⋯）

大野晶がそこで立ち止まり、必死に視界の回復を祈る。どれくらいの時が経過したのか、次に目を開いた時、視界にようやく色彩が戻っていた。
彼は即座に男子トイレへ入ると、盛大に胃液を吐き出す。昨日はＸＸと酒を飲んでいた事もあり、吐くこと自体は別に可笑しな話でもない。

「はぁ⋯⋯俺も歳なんかね」

吐き終えた後、鏡を見ながらポツリと呟く。
彼は當時まだ２０代であったが、酒はそれなりに強く、翌日に吐くなど余り経験がなかったのだ。顏を洗ってうがいをし、改めて鏡を見つめる。

「うん、拔群に良い男だわ。その上、俺は天才ときてる」

一人だから言いたい放題である。
いや、これから会う人物の事を考えれば、そうでも思わなければとてもまともに相對しえないと思っているのだろう。

──ミキティ☆

大野晶が運營するＧＡＭＥに、早くから参加していた古参玩家である。
後から知った現世での肩書きは、海外の有名遊戲会社「Ｘ２—XMX」の專務。
一介の社会人でしかない大野晶からすれば、完全に雲上人であった。

「な～にが大規模MMOだ。著作権料として１億ぐらい払うなら考えてやんよ」

大野晶がそう嘯いたが、心底そんな事を思っている訳ではない。
むしろ、この話の裏側にあるのは何か、とばかり考えている。はっきり言って、海外の遊戲会社が、こんな個人のちっぽけな遊戲に興味を示したのも異常だし、まして、そのちっぽけな個人相手に詐欺をやるというのも考えにくい。

それこそ、本當に興味が出たのなら自分達で作れば良いのだから。
唸るような人材と、有り余る財力が向こうにはある。世界全体から見れば、極東の島国の、それもちっぽけな個人遊戲になど誰が遠慮するだろうか。

「言っとくけど、俺は金なんて持ってねぇからな」

悲しい呟きであったが、事實である。
宵越しの金を持たない彼を詐欺にかけようとしても、出てくるのは耳を塞ぎたくなるような罵詈雑言と、酷い煽りだけであろう。

「──行くか」

頬を叩き、大野晶が廊下へと戻る。
先程は捻じ曲がっていた廊下が、今度はまともに映った。
長い廊下を颯爽と歩き、目當てのドアをノックする。現れたのは、片眼鏡を付けた老紳士であり、執事服を着た外人であった。

「あー、えっと、大野晶ですけど。あの、日本語は通じ──」
「ようこそ、大野樣。お待ちしておりました」

老紳士の口から、流暢な日本語が返ってきた。
しかも、相手は見ているだけで冒しがたい氣品を纏った老人であり、大野晶は頭の片隅で「こりゃヤバイな」と直感する。
自分とは、人生経験も何もかもが違いすぎる相手だと確信してしまったのだ。

「⋯⋯その、ミテキィさんはいらっしゃいますか？」
「はい、私（わたくし）がそうでございます」
「えっ？」

數時間後──

決して短くない對談を終え、大野晶がＸＸの家へと戻る。ホテル代をケチったのか、滞在中はここをホテル代わりに使っているらしい。
泊めて貰っている割には、隨分と偉そうな態度ではあったが。

「晶、話はどうだったん？」
「んー、マジでデカい遊戲にしたいんだってさ」
「すっげぇじゃん！金入ったらウチにも奢って！」
「働けよ」
「無理無理！こっちは息するだけで大變なんだからさー。どうしてこの苦勞が分からないかなー。肺動かして、心臓まで動かしてんだよー？　すげぇ勞働じゃん」
「赤ん坊でもやってるからな」

後に魔王と呼ばれるだけあって、その口から出る言葉は辛辣極まりない。
晶が足元に溢れ返っているゴミを袋に入れ、キッチンへと放り出しながら、溜息をつく。掃除した部屋が、また散らかっているのだ。

「何をどうしたら、一日でこんなに汚せるんだよ」
「ぇー、必要なもんを出そうとしたらグチャーってならない？」
「グチャグチャなのはお前の人生だろ」
「ひどっ！晶ってば本當に屑ね！このポリバケツ！ポリエステル！」
「最後の、ただの合成繊維だからな」

ビールの蓋を開けながら晶が目をやると、ＸＸはまたしても妙な遊戲を立ち上げ、それをプレイしていた。ブラウザの中央には黑い渦があり、そこに全てが呑み込まれていく内容である。

人も、馬も、豚も、車も、マンションも──

何もかもが、その黑い渦に呑みこまれていく。
端的に言って、氣味が惡い光景であった。

「それも、お前の自作遊戲か」
「んー、何だろ。予言みたいな？」
「はぁ？」

晶がビールを傾け、鼻で笑う。
予言だの、夢占いだの、守護靈だの、手相だの、この男はそれらの類を頭から受け付けない。自分の人生は自分で切り開くものであり、そんなものを信じる必要がない、と思っているのだ。

この男から言わせれば、手の皺で人生で決まるなら、才能も努力も運も、何もかもが無意味であり、そんな馬鹿げた話はないといったところである。
これは、挫折を知らない人間にままあるタイプであろう。自分の努力と才能で、何もかもを叩き伏せていく自信があるのだ。

「晶は確かに異常だけどさー、大きな渦には勝てないと思うよ」
「大きな渦って何だよ」
「すぐそこに来てるよ──ブラックホール」
「何だそりゃ」

いつものオカルトか、と晶が輕く聞き流す。
ＸＸは幽靈や超常現象などが大好きであり、この部屋にも自殺した靈が住んでいるなどと言っていたのだ。

「で、自殺した靈とやらは何処に居るんだ？　まるで見かけないが」
「あぁ、晶が怖いみたいでどっか行ってるみたい」
「俺が何をしたんだよ⋯⋯」
「あのオッサン、ホモなんだけど晶は專門外みたいだわ」
「オッサンの靈かよ！しかもホモかよ！」

晶が盛大に突っ込んだが、ＸＸは笑うばかりであり、とりとめもない。
画面の中では木々や子供達、魚や試験管などよく分からない物まで黑い渦に吸い込まれていき、渦はどんどん巨大化していく。

「この渦、何がしたいんだ？」
「さぁ、ウチにも分からないんだよね」
「で、またエンディングも何も無しか？」
「ううん、一杯吸い込んでるし、最後は爆發するんじゃね？」
「──結局、爆發オチかよ」
「『だぁっはっはっ！』」

二人が大口を開けて爆笑し、缶ビールを叩いて乾杯する。
しかし、後にＸＸの予言は不氣味なまでに的中した。

時は２００４年──

日本中のネット社会を全て飲み込む、一つの怪物が産声を上げた年である。
これより後、あらゆる個人サイトが消滅していき、ブログは廢れ、個というものは埋没し、全てがその一点へと集まっていく事となった。
それはＸＸの言った、ブラックホールそのものであったのかも知れない。

「あー、原宿とかにミサイル落ちねぇかなー」
「働け。んで、ミサイル買え」
「東京タワーが倒れたりとかさー」
「働け。んで、ノコギリ買え」
「切る？　切っちゃう？　倒しちゃう？」
「さて、１１０番っと⋯⋯」
「ちょ、晶！お前、マジで押してるだろ！」

画面は暗転し、デスクトップの樣々なアイコンまで吸い込まれていきます。

吸い込むものが無くなったのか、それとも飽きたのか。

呑み込まれた先にあったのは、強制リセット。

皆、同じスタートラインに立ちます。

いい歳をしたあの人も、可愛いあの娘も、新入生の面持ちでした。

入學おめでとう。編入おめでとう。

世界はやがて爆發します。

卒業おめでとう。

全員、平等にくたばりやがります。