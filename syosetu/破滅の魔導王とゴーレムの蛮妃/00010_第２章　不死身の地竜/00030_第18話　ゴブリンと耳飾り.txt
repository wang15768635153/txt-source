如同雪一樣白的希臘雕像，將柔軟的腳高高地舉到頭上。
好厲害，像個芭蕾舞舞者。
然後她的腳一口氣踢在大猿的腦門上。
連猴子的腦殼上的岩石都粉碎了。
血沫橫飛，碎裂的岩石像煙花一樣爆炸飛舞。

雖然哥雷下手的動作很華麗，但她僅僅是將其頭骨踢落而已。
破碎的頭骨掉落至地面，大猿凋零。
果然猴子的尺寸，隨著向西前進，會越來越大。
這一帶的猿猴，已經便乗了超級大猴子。從尺寸上來說，可以說是大猩猩。
嘛，正如所見，在哥雷面前還是平等的一擊必殺啊。
或者說，今天早上偶然決定掉落的時候，我誇獎了一下之後，
已經超過半天以上，一直只使用腳和陷阱……。

我覺得任何事情的平衡都很重要。
如果不馬上表揚其他的話，好像不太好。

「話雖如此，ネマキ君的哥雷姆還是很厲害的。
說實話，沒想到會達到這種程度……」

{不知道是甚麼所以先保留，之後會修正
ネマキ君是主角的樣子}
一邊重新戴上快要掉下來的圓形眼鏡，スペリア的大叔茫然地嘟噥著。

「真的沒想到在土小鬼橫行的街道上就這樣直行啊。
我一直以為，一定是利用哥雷姆的表土索敵，在回避和群體接觸的同時前進的……」

{表土:指表層的泥土}

土小鬼哥布林
是啊，這群猴子的正式名稱。
本以為是纏繞著岩石的日本猴子，沒想到這些傢伙是哥布林……。
當然，我已經放棄了對這些不可思議的生物深入的思考。
如果說スペリア的大叔說是哥布林的話，那一定就是哥布林吧。
但是各位，請稍等一下。

因為哥布林很像人，恐怕分類上是靈長類的同類吧。
如果把人類以外的靈長類看作猴子的話，那麼可以說哥布林在地球上也算是猴子吧？
是的。也就是說，哥布林是猴子。
因此今後，我心中把這些生物的稱呼統一為「猴子」。

「土小鬼，用驅除的難度來說，無疑是最麻煩的一種魔獸。
覆蓋著他們的體表的岩石，會干涉魔術。所以，對於魔術師來說是相當棘手的存在」

面對一副不可思議表情的我，斯伯利亞的大叔給我解釋了很多。
果然很有教職的感覺。

「再加上，土小鬼使用的「魔導」是麻煩的土魔導「岩彈」。

通常，即使對魔獸使用的技能相性良好的哥雷姆，也無法有良好的成效。
因為〈岩石彈〉伴隨著質量呢。
輕型魔像的裝甲就如同紙一樣地輕易貫穿了，
即使軍用的重型魔像，被包圍也施加了飽和攻擊，也無可奈何」

說完這句話的斯伯利亞大叔，稍稍看著哥雷看了之後繼續說道。

「話雖如此，但如果能以和內馬凱君的葛蕾黛侖一樣的索敵能力和速度，
而且超強力的物理攻擊先發制人的話，這和內馬凱完全沒有關係。」

{這裡應該是指前一話大叔稱讚男主的話

「使役的格雷姆的實力，也就等同於格雷姆使的實力。
既然“葛蕾黛侖”的性能非常出色，那就證明內馬凱君也很優秀。」}
嘛，事到如今，姑且不論哥雷的胡鬧，
從這些事來看，教堂裡的格雷姆們相當強啊……。
ん？算了。現在大叔的解說裡有相當讓人在意的詞語呢，魔導？
猴子射過來的石頭，不是魔術嗎？
或者說，魔導是那個吧。完全忘記了，不過，
是在我的這個世界的大致的頭銜「破滅的魔導王」的「魔導」的事吧？

「……所謂魔導？和魔術不一樣嗎？」

對於我的問題，斯伯利亞的大叔給我的解釋。


「魔導是指只有魔獸才能使用的魔力操作術。
已經生成後的魔術，從那裡，還能加以引導操縱。
我們人類，不能再操縱已經生成了的魔術了吧？
{私たち人間は、一度生成させた魔術をそれ以上操るなんて事はできないだろう？}
譬如，如果放過一次火彈和風刃，那個以後的軌道不能變更。
特別是土魔術，因為如果生成了石頭就只能一直放在那裡，所以非常悲慘」

くっ……！不甘心！但是，不能否定！

「但是，魔獸使用被稱為‘魔導核’的器官，
能夠操作生成後的魔術。這就是魔獸、因為是魔獸吧。」

{これが魔獣の、魔獣たるゆえんだね}

「原來如此。所以他們把生成的石頭射過來這件事
是土魔術不可能做到的事嗎……」
「就是這麼回事啊。歸根結底，魔導的情況與人類使用的魔術相比，

各屬性的危險性有很大程度的不同。特別是伴隨質量的土屬性和冰屬性，
能貫穿防禦的魔術，變成非常麻煩且危險的屬性。」

一邊並排走一邊解釋了一陣子之後，，大叔把臉轉向了我。
然後，像是嘲笑成績差的學生一樣，一邊苦笑一邊拍著我的肩膀。

「話說，內馬凱君就是那個啊。明明使役著這麼厲害的格雷姆，魔獸的知識卻很不了解啊！
呵呵，看起來是個相當了不起的少爺呢。」

真丟臉。我的知識，大概是這個世界的小學生以下……。
話雖如此，“魔導”啊。
我的<NTR>，讓猴子頭上的石頭嗖嗖地飛，那不是魔導嗎？
這麼說來，今天因為哥雷用陷阱和雙腿一個接一個地殺猴子的原因，
一次也沒有使用<NTR>。

「那個，除了魔獸以外，都無法使用魔導嗎？」

總之，有困難的時候先問問斯伯利亞老師吧。

「嗯我從來沒聽說過這樣的事情。」

回答了這個問題的他，露出了稍微思考的表情之後，又繼續說下去。

「……如果魔獸以外，能使用魔導，是那個。大概也就只有傳說中出現的“魔導王”吧。
嘛，被懷疑是否為實際存在的存在」

魔導王。

來到這裡，從他的嘴裡說出來，我的，暫且在這個世界的頭銜。
真的假的 你知道嗎，斯伯利亞大老師！
我正想進一步提問有關魔導王的時候，
一個茶色的影子從前方沿著街道的懸崖上跳了下來。
影子的原形，是大猩猩尺寸的大猴子。
距離比較近。
{意★義★不★明
わりと距離が近い。}
我看了旁邊的哥雷的樣子。我站在我的斜前不管什麼時候都能保護我的位置，
卻絲毫沒有要襲擊敵人的跡象。略微介意著我。
啊，原來如此。大概是像往常一樣讓我用<NTR>玩耍，
故意允許了單獨行動的猴子的接近吧。真是個溫柔的傢伙。
說起來，我從剛才開始就一直丟下哥雷，只和斯伯利亞大叔說話啊。
也許是變得寂寞，想要用盡全力去吸引我吧。
這傢伙，比較不擅長自我主張。

但是，該怎麼說呢?從剛才的斯伯利亞大叔的話來看，
總覺得<NTR>，好像是只有魔導王才能使用的技能呢？
在這裡使用也沒關係嗎？
難得的擔心，不過，請哥雷普通的打倒比較好吧。
我環視著前方的大猴子。距離很近，
猴子本身也很大，所以可以看到體表粗糙的岩石。

「話說回來，那塊石頭會反彈魔術嗎？看起來像一塊普通的岩石。」

聽到我喃喃自語的斯伯利亞大叔說。

「土小鬼的魔術防禦，原理上和格雷姆的魔術完全一樣。
是啊，百聞不如一見。讓我來演示一下。」

大叔這麼一說，就邁出了一步。
然後，他向靠近右手的大猿猛地擺出姿勢。
而大猿則絲毫沒有介意他的動作。慢慢地靠近。
在充分吸引大猿的時候，大叔詠唱了魔術。

「那麼，開始吧。――〈火球〉ッ！」

壘球般大小的火球，從大叔的手掌高速射出。
火屬性魔術
非常華麗，很有主人公的感覺。真不甘心。
火球漂亮地直擊了猴子的胸口。
但是著彈的瞬間，有種奇怪的感覺。
撞到猴子的體表的火球，看上去像是擴散後消失了。

「……嘛，就這樣，被石頭內部的魔力迴路彈開，
被分散的魔術會恢復到粒子態。」

{散らされた魔術が粒子に戻ってしまうわけだね}

大叔那種解說的樣子，感覺簡直就是理科老師。
被火魔術的熱產生的熱氣包圍了的大猴子，慢慢地走向了大叔的前方。

「果然對這個尺寸的變異體來說，只有中級魔術這種程度絲毫沒有影響了……」

看來，猴子確實沒有受到傷害的樣子。
原來如此，真的。魔術沒用啊。
明明是相當高溫的火球。好厲害的大猴子。
話說回來，剛才大叔特地等猴子接近後才放出魔術。
這個世界的魔術，也許射程距離意外的短。
斯伯利亞的大叔回頭看了深深地佩服實驗情況的我。
他不知怎麼地不好意思開口了。

「那個，內馬凱君。因此，我的魔術是打不倒的。
不好意思，能打倒那個土小鬼的變異體嗎？」
「啊！是這樣啊。很抱歉我太不機靈了。」

對了。試驗的事後處理由我方負責。
看去，被火魔術擊中的大猿的表情，完全生氣了。
氣得直咬牙咧嘴，眼看就要撲過來了。
糟糕了，這個。

「哥雷，能幫忙幹掉那隻猴子嗎？」

聽到我的話後，像一隻飢餓的獵犬一樣，襲擊了獵物。

在美麗的腳後跟落下時，一瞬間大猴子的頭部變成了骯髒的煙花。

-----

「看吧，內馬凱君。那是土的瘴氣。濃度已經達到可以目視的程度」

斯伯利亞的大叔指向了前進方向的地平。
一看，街道的盡頭被薄薄的灰霧一樣的東西覆蓋著。
如果沒有大叔來解說的話，
我恐怕會去推測為甚麼沙塵會變成那樣的狀況，而浪費的勞力吧。

「雖然對人體沒有直接的危害，但不只會使魔獸大量出現，還會使之變異。
土質也變質了，農作物也長不下去了。
……如果大地上充滿了瘴氣，人就不能住在那裡了。」

真的假的，土屬瘴氣看起來倒挺樸素的啊。

「不過，在這個地方發生土屬瘴氣，從來沒見過那樣的記錄。
肯定是發生什麼異常情況了。」

所以斯伯利亞的大叔特意冒險到這種地方來調查的嗎？
真是了不起的精神啊。這位大叔正是文化人的楷模。
作為地球的文化人代表，在心中向大叔表示敬意的我。
那時，我看到了前面瘴氣的霧靄中幾個大影。
仔細一看，建築物……村落。
天已經快黑了。今晚會在這裡住宿吧。
村落果然無人。說來也是理所當然的。
在這一帶，猴子的尺寸已經不妙了。
不管怎麼想，都完全超過了能對付普通人的水平。

「果然這個村子也被瘴氣吞沒了嗎……」

斯伯利亞大叔面帶難色地嘟囔著。
あれ？大叔不知道這個村落的情況嗎？
他不是經過這條街道來到撒瑪利亞了嗎？
我們來到東邊的方向，沒有一個村落，街道也沒有特別分岔，是東西方的一條大道。
所以，大叔一定是從和我們相反的西方走過來的吧。

「斯伯利亞先生到底是怎麼來到撒瑪利亞的村落的？」

受到我的疑問的斯雷利亞大叔指著自己戴在右耳的藍綠色耳環。

「嘛，你看看吧。」

斯伯利亞大叔站在村落的入口處，小聲念起。
戴在右耳的藍綠色水晶，開始發出淡淡的光芒。
幾秒鐘後，以他為中心，颳起了微風。
微風的感覺。
{そよ風ってかんじだな。}
雖然他暫時閉著眼睛，但不久他慢慢地睜開眼睛，點了點頭。

「……嗯。不要緊吧。這個村落的周圍，已經沒有土小鬼了。
剛才哥雷打倒的個體，好像是最後一隻」

斯伯利亞大叔的水晶耳環似乎是一種魔道具。
用這個強化風魔法，進行著高度的索敵。據說是大叔的必殺技。
用這個索敵術躲開猴子，從南邊山中曲曲折折地北上，才到達了撒瑪利亞的村落。
確實在地圖上看到的感覺，南方也有村落的樣子。
但是，因為距離很遠，而且必須翻山越嶺，所以我首先從候選中剔除了(南方村落)。
{註解:這裡是指男主在原本的盆地尋找方向時的選項}
他就是那個距離，一個勁兒地躲開猴子一個人移動過來的嗎？
而且，在連路都沒有的險峻山地。
和完全被背抱在背上的我，是截然不同的。
斯伯利亞大叔的努力使我抬不起頭來。

「這個魔道具，本來是兩隻耳朵的一套……好像一隻掉在哪裡了。
大概不小心遇上了土小鬼，而逃跑的時候吧。」

斯伯利亞的大叔這樣說。
他露出有點為難的表情，一邊撓頭，一邊害羞地苦笑。
{大叔這樣一點都不萌啊(ノ▼Д▼)ノ}
所以，術的精度比本來的要低很多啊。
老實說，你能陪我去真是幫了大忙了。
大叔真辛苦。
而且為了學術調查這個非常文化的崇高目的。
如果我能幫上一點忙，那真是太好了。
嘛，雖然我只是個哥雷行李而已，但希望你不要說破這點。
{大致上應該是這樣
原文:まぁ、俺はただのゴレのヒモ狀態なんだが、
そこには觸れないでほしい。}
我們適當地修補了一間空房，今晚就住在那裡。
晚飯時，我們以各自帶著食物做了湯。
斯伯利亞大叔從揹包裡輕輕地拿出來，總覺得像洋蔥之類的東西，像小雞豆之類的。
恐怕是因為調查比預定提前結束的原因，沒必要太在意剩餘的食物吧。

我為了與其對抗，(我)提供了大量的乾肉。
將拍打後鬆開的肉和蔬菜一起放進鍋裏。

在那裡，大叔拿著的香辛料一口氣放進去了。
真香啊。
和大叔一起好好地吃了煮軟了的配料的湯。
啊，味道還不錯。
乾肉放進湯裡，出乎意料的是好的湯汁。
是叫ジャーキー湯的東西。
{意★義★不★明
ジャーキースープってやつである。}
啊，這種蔬菜是普通的洋蔥啊。
好吃。
多虧了今天走得這麼辛苦，湯的鹽氣充分滲透到空腹的身體裏，
這又變得格外有風味了。
順便說一下，這個湯中使用的水，是斯伯利亞大叔用水屬性魔術製成的。
這個大叔，真是多才多藝啊。
即使那樣，令人羨慕啊水魔術。
與倒塌後像垃圾一樣的土不同，即使水倒塌也是水。
這樣能喝好。
太棒了。
{指失去原本的形狀吧?}
其實這個水魔術，據說如果被專業的人使用的話，是相當強有力的。
伴隨質量的內容魔術這個點，與冰內容和土內容相同，
不過，水內容因為生成物不是個體，那個性質包含流體操作。
也就是說，和火魔術、風魔術一樣，
即使是人類，在生成前也可以指定移動向量。
{大概是指因為沒有固定形狀所以可以射出吧?}
總而言之，可以發射水槍，如果順利的話，就可以在一定程度上操縱河流。
甚至有水屬性大魔術師一個人攻陷城池的記錄。
而且，治癒魔術也有8成以上屬於水魔術.
水魔術好厲害。
雖然是那麼厲害的水屬性，但是沒有包含在四大元素屬性中。
這個世界的四大元素是火、風、土、冰。
也就是說，水屬性，別說是被當作垃圾雜魚屬性的土屬性了，
與被認為是以大氣中的水的粒子為媒介使用的相似系統的冰屬性相比，
公認的地位是較低的。
總覺得有點不走運。
我帶來的缸中的水也減少了，順便用水魔術補充了
這個村落遺跡也有井，雖然沒有枯萎，但是受瘴氣的影響，
水質可能發生了變化，所以最好不要飲用。
如果不經意地喝的話，可能會鬧肚子。
水魔術雖然很樸素但超方便啊。
謝謝你，水魔術。
一起合作，總有一天能打倒火魔術吧。
一邊望著滿是零星美麗的水瓶，一邊在心中發誓要和水魔術結盟。

------

暖爐的火發出了溫暖的光芒。
飯後，我一邊大口地咬著格蘭莓蘋果大人，一邊重新讀《魔法入門I》。

今天我從大叔的課上知道了各種各樣的新情報。
如果現在再讀一次，這本書的理解也許也會加深。
凡事重溫都很重要。
斯伯利亞大叔坐在壁爐前，正在檢查裝置。
順便說一下，我和大叔在各種各樣食物上分享(交流)，但是，
只有ランベリー蘋果大人，很頑固的沒有分享。
對不起，大叔饒了我吧，雖說是同為文化人的友人，
唯有這點是無論如何也不能讓步的。
那麼，本來開始認真學習的我，卻因為吃飽了而且很溫暖，
(所以)已經開始迷迷糊糊的看書。
以讀書家自居的我來說，是不應該有的失態。
因為，沒辦法啊。旁邊坐著的哥雷(老婆)，非常暖和。
完全是溫泉(?)。不，這種溫暖的魔力，倒不如說更接近被爐。
{完全に湯たんぽ……いや、この溫もりの魔力は、むしろこたつに近い。}
迷迷糊糊的我的側臉，從近處凝視著，哥雷的濃烈而熱乎乎的視線。
但是，我已經習慣了以往的事了。
睡魔是不會那樣離開的。
突然，斯伯利亞大叔對著神志低落的我搭話了。

「是『魔術入門』嗎？好懷念啊！當我還是初學者的時候，我學到了很多東西。」

聽到大叔的聲音，我揉著睏倦的眼睛，抬起頭來。
他仔細地看著我手邊的書，高興地眯起了眼睛。
但是，他那懷念的眼神……為什麼呢？看上去很遠很寂寞。

「……這樣啊。你讀了那本書，在學習魔術。」

我還有點迷迷糊糊的。
但是他剛剛的發言足以讓我從夢境中一鼓作氣地恢復原狀。

「那本書的作者不是叫露貝烏·扎伊倫嗎？
那個人，是我的魔術師傅。」
